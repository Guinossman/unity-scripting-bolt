using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{ 
    [SerializeField]
    private Vector3 Axis;
    public float scaleUnits;


void Update() {
        Axis = CapsuleMovement.ClampVector3(Axis);
        transform.localScale += Axis * (scaleUnits * Time.deltaTime);

    }
}